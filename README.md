# distributed-lock-learning

#### 介绍

1. zookeeper-LeaderLatch选举主节点实现
2. redis-Jedis缓存实现
3. database-mysql乐观锁实现
4. zookeeper-InterProcessMutex可重入排它锁实现
5. zookeeper-InterProcessSemaphoreMutex不可重入排它锁实现
6. zookeeper-InterProcessReadWriteLock读写锁实现
7. zookeeper-LeaderSelector选举主节点实现
