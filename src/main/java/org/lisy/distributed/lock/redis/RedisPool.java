package org.lisy.distributed.lock.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public final class RedisPool {
	
	/**
	 * 服务器IP
	 */
	private static String ADDR = "127.0.0.1";
	/**
	 * 端口号
	 */
	private static Integer PORT = 6379;
	/**
	 * 访问密码
	 */
	private static String AUTH = null;
	/**
	 * 超时时间
	 */
	private static Integer TIMEOUT = 10000;
	
	/**
	 * 可用连接实例的最大数目，默认值是8
	 */
	private static Integer MAX_TOTAL = 1024;
	/**
	 * 最大空闲连接数，默认值是8
	 */
	private static Integer MAX_IDLE = 200;
	/**
	 * 获取连接时的最大等待毫秒数(如果设置为阻塞时 BlockWhenExhausted)
	 * 如果超时就抛异常 JedisConnectionException，小于零:阻塞不确定的时间，默认-1，表示永不超时
	 */
	private static Integer MAX_WAIT_MILLIS = 10000;
	/**
	 * 在获取连接的时候检查有效性，进行 validate(验证)操作
	 * 如果为 true，则得到的 jedis 实例均是可用的，默认 false
	 */
	private static Boolean TEST_ON_BORROW = true;
	
	/**
	 * 单例
	 */
	private static JedisPool jedisPool = null;

	/**
	 * 静态块，初始化 Redis 连接池
	 */
	static {
		try {
			JedisPoolConfig config = new JedisPoolConfig();
			/**
			 * setMaxActive 和 setMaxWait 属性在高版本已弃用，如:2.9.0
			 */
			config.setMaxTotal(MAX_TOTAL);
			config.setMaxIdle(MAX_IDLE);
			config.setMaxWaitMillis(MAX_WAIT_MILLIS);
			config.setTestOnBorrow(TEST_ON_BORROW);
			jedisPool = new JedisPool(config, ADDR, PORT, TIMEOUT, AUTH);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 获取 Jedis 实例
	 * 
	 * @return
	 */
	public synchronized static Jedis getJedis(){
		try {
			if (jedisPool != null) {
				Jedis jedis = jedisPool.getResource();
				return jedis;
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 关闭连接
	 * @param jedis final 只读
	 */
	public static void returnResource(final Jedis jedis){
		if (jedis != null) {
			// 旧: jedisPool.returnResource(jedis)
			jedisPool.close();
		}
	}
}
