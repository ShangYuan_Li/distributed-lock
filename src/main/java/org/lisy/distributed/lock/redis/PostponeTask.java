package org.lisy.distributed.lock.redis;

public class PostponeTask implements Runnable {

	private String key;
	private String value;
	private long expireTime;
	private boolean isRunning;

	public PostponeTask() {
	}

	public PostponeTask(String key, String value, long expireTime) {
		this.key = key;
		this.value = value;
		this.expireTime = expireTime;
		this.isRunning = Boolean.TRUE;
	}

	@Override
	public void run() {
		// 线程等待多长时间后执行
		long waitTime = expireTime * 2 / 3;
		while (isRunning) {
			try {
				Thread.sleep(waitTime);
				if (JedisLock.postpone(key, value, expireTime/1000)) {
					System.out.println("lock postpone: " + System.currentTimeMillis());
				} else {
					this.stop();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void stop() {
		this.isRunning = Boolean.FALSE;
	}

}
